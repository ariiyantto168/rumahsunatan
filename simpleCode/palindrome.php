<!-- 1)	Buatlah program untuk mengetahui apakah teks tersebut palindrome,
yaitu teks apabila dibaca mundur (reverse) dan dibaca maju akan tetap sama. 
Contoh : teks ‘civic’ tetap sama apabila diputar balikkan.
Program ini dibuat menggunakan Bahasa PHP, tanda baca dan symbol dihiraukan
Ketika menjalankan program tersebut. Contoh : teks ‘$civ%i#c’ akan tetap menjadi teks ‘civic’ -->


<?php 

function palindrome($kata){
  $result = preg_replace("/[^a-zA-Z]/", "", $kata);
  if($result == strrev($result)){
      echo " ". $result;
  }else{
      echo " ". $result." "."bukan palindrom";
  }
}


echo palindrome('katak*1-09865#4@!-_=+=``');
echo palindrome('kasur rusak');
echo palindrome('mister');

