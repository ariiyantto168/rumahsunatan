<!-- 3)	Buatlah sebuah program untuk mengetahui berapa tahun, bulan,
 hari (angka) dan nama hari nya dari tgl lahir sebagai parameternya. -->

 <?php

function hitungumur($tanggal){
  $day = date('D', strtotime($tanggal));
  $dayList = [
    'Sun' => 'Minggu',
    'Mon' => 'Senin',
    'Tue' => 'Selasa',
    'Wed' => 'Rabu',
    'Thu' => 'Kamis',
    'Fri' => 'Jumat',
    'Sat' => 'Sabtu'
  ];

  $tanggal_lahir = new DateTime($tanggal);
  $sekarang = new DateTime("today");

    if ($tanggal_lahir > $sekarang) { 
        $thn = "0";
        $bln = "0";
        $tgl = "0";
    }
    $thn = $sekarang->diff($tanggal_lahir)->y;
    $bln = $sekarang->diff($tanggal_lahir)->m;
    $tgl = $sekarang->diff($tanggal_lahir)->d;
    return "lahir hari ". $dayList[$day].", " .$thn." tahun ".$bln." bulan ".$tgl." hari";
}

echo hitungumur("1995-03-22");

