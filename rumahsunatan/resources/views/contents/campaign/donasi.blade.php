<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">History Donasi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Campaign</li>
              <li class="breadcrumb-item active"><a href="{{ url('donasi/pengguna') }}">Campaign</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Index</h3>

            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Donatur</th>
                          <th>Jenis Biaya</th>
                          <th>nominal</th>
                          <th>Tanggal Donasi</th>
                          <th>Via</th>
                          <th>Tipe</th>
                          <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($donasi as $idx => $don)
                      <tr>
                          <td>{{ $idx+1 }}</td>
                          <td>{{ $don->nama_donatur }}</td>
                          <td>{{ $don->jenisbiaya->nama }}</td>
                          <td>{{ $don->nominal }}</td>
                          <td>{{ $don->tgl_donasi }}</td>
                          <td>{{ $don->by }}</td>
                          <td>{{ $don->tipe }}</td>
                          <td>
                            @if($don->status)
                              <span class="btn btn-block btn-success btn-sm" style="width:70px;">Active</span>
                            @else
                              <span class="btn btn-block btn-danger btn-sm" style="width:70px;">Inactive</span>
                            @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>                    
                </table>
            </div>
        </div>  
    
      </div>
    </section>
  </div>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
  });
</script>
