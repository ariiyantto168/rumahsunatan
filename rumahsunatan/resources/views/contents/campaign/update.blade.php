<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Campaign</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="{{ url('campaign') }}">Campaign</a></li>
              <li class="breadcrumb-item active">Create-new</li>
            </ol>
          </div><!-- /.col -->      
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create</h3>
            </div>
            <div class="card-body">
              @include('contents.allmessage')
                <form role="form" class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('campaign/update/'. $campaign->id_campaign) }}">  
                  @csrf
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Campaign</label>
                        <div class="col-sm-5">
                          <input type="text" name="nama" value="{{$campaign->nama_campaign}}" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Campaign</label>
                        <div class="col-sm-5">
                        <select name="id_jenis" id="select2" class="form-control" required>
                              <option value=""> -- select jenis -- </option>
                            @foreach ($jenis as $jen)
                                <option value="{{$jen->id_jenis}}" @if ($jen->id_jenis == $campaign->id_jenis) 
                                    selected
                                @endif>{{$jen->nama}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Volunteer Campaign</label>
                        <div class="col-sm-5">
                        <select name="id_volunteer" id="select2" class="form-control" required>
                              <option value=""> -- select volunteer -- </option>
                            @foreach ($volunteer as $vol)
                                <option value="{{$vol->id_volunteer}}" @if ($vol->id_volunteer == $campaign->id_volunteer) 
                                    selected
                                @endif>{{$vol->nama_volunteer}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Kategori Campaign</label>
                        <div class="col-sm-5">
                        <select name="id_kategori" id="select2" class="form-control" required>
                              <option value=""> -- select kategori -- </option>
                              @foreach ($kategori as $kat)
                                <option value="{{$kat->id_kategori}}" @if ($kat->id_kategori == $campaign->id_kategori) 
                                    selected
                                @endif>{{$kat->nama}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Program Campaign</label>
                        <div class="col-sm-5">
                        <select name="id_program" id="select2" class="form-control" required>
                              <option value=""> -- select program -- </option>
                            @foreach ($program as $pro)
                                <option value="{{$pro->id_program}}" @if ($pro->id_program == $campaign->id_program) 
                                    selected
                                @endif>{{$pro->nama}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Total Biaya</label>
                        <div class="col-sm-5">
                          <input type="number" name="total_biaya" value="{{$campaign->total_biaya}}" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Tanggal berjalan kegiatan</label>
                        <div class="col-sm-5">
                        <input class="form-control" id="date" name="tgl_berjalan_kegiatan" value="{{$campaign->tgl_berjalan_kegiatan}}" type="text"/>
                        </div>
                    </div>
                        
                    <div class="form-group">
                      <label for="exampleInputEmail1">Tanggal Berahkir kegiatan</label>
                        <div class="col-sm-5">
                        <input class="form-control" id="date" name="exp_date" value="{{$campaign->exp_date}}" type="text"/>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="name_materi" class="col-sm-2 col-form-label">Description Campaign</label>
                      <div class="col-sm-5">
                        <textarea class="form-control" rows="5" id="description" name="deskripsi"  cols="10" rows="2" required>{{$campaign->deskripsi_campaign}}</textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name_materi" class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-5">
                        <input type="checkbox" name="status" checked>
                      </div>
                    </div>
                        
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary" value="upload">Submit</button>
                    </div>
                </form>
            </div>
        </div>  
    
      </div>
    </section>
  </div>

  {{-- batasan size images --}}
<script type="text/javascript">
  $(document).ready(function() {
    maxFileSize = 10 * 1024 * 1024 / 2; // 10 mb
    imageSize = maxFileSize / 10;

    $('#images').change(function() {
      fileSize = this.files[0].size;

      if (fileSize > imageSize) {
        this.setCustomValidity("You can upload only files under 1 MB");
        this.reportValidity();
      } else {
        this.setCustomValidity("");
      }
    });
  });
</script>
