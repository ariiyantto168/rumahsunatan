<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Jenis Campaign</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="{{ url('campaign/jenisbiaya') }}">Jenis</a></li>
              <li class="breadcrumb-item active">Update</li>
            </ol>
          </div><!-- /.col -->      
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Update</h3>
            </div>
            <div class="card-body">
              @include('contents.allmessage')
                <form role="form" class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('campaign/jenisbiaya/update/' . $jenis->id_jenis_biaya) }}">  
                  @csrf
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                        <div class="col-sm-5">
                          <input type="text" name="nama" value="{{$jenis->nama}}" class="form-control" required>
                        </div>
                    </div>
                        
                    <div class="form-group">
                      <label for="name_materi" class="col-sm-2 col-form-label">Description </label>
                      <div class="col-sm-5">
                        <textarea class="form-control" rows="5" id="description" name="deskripsi"  cols="10" rows="2" required>{{$jenis->deskripsi}}</textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name_materi" class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-5">
                        <input type="checkbox" name="is_active" checked>
                      </div>
                    </div>

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary" value="upload">Submit</button>
                    </div>
                </form>
            </div>
        </div>  
    
      </div>
    </section>
  </div>

  {{-- batasan size images --}}
<script type="text/javascript">
  $(document).ready(function() {
    maxFileSize = 10 * 1024 * 1024 / 2; // 10 mb
    imageSize = maxFileSize / 10;

    $('#images').change(function() {
      fileSize = this.files[0].size;

      if (fileSize > imageSize) {
        this.setCustomValidity("You can upload only files under 1 MB");
        this.reportValidity();
      } else {
        this.setCustomValidity("");
      }
    });
  });
</script>
