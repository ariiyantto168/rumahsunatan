<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Volunteer Campaign</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="{{ url('campaign/volunteer') }}">Volunteer</a></li>
              <li class="breadcrumb-item active">Create-new</li>
            </ol>
          </div><!-- /.col -->      
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create</h3>
            </div>
            <div class="card-body">
              @include('contents.allmessage')
                <form role="form" class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('campaign/volunteer/create-new') }}">  
                  @csrf
                  <div class="form-group">
                      <label for="exampleInputEmail1">Nama volunteer</label>
                        <div class="col-sm-5">
                          <input type="text" name="nama_volunteer" placeholder="Example: Ariyanto" class="form-control" required>
                        </div>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Type Volunteer</label>
                        <div class="col-sm-5">
                        <select name="tipe" id="select2" class="form-control" required>
                              <option value=""> -- select type -- </option>
                              <option value="volunteer">volunteer</option>
                              <option value="pribadi">pribadi</option>
                            </select>
                        </div>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">NPWP</label>
                        <div class="col-sm-5">
                          <input type="number" name="npwp" placeholder="Example: 98988.." class="form-control" required>
                        </div>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Telepon</label>
                        <div class="col-sm-5">
                          <input type="number" name="tlp" placeholder="Example: 98988.." class="form-control" required>
                        </div>
                  </div>
                  <div class="form-group">
                      <label for="name_materi" class="col-sm-2 col-form-label">Alamat Volunteer</label>
                      <div class="col-sm-5">
                        <textarea class="form-control" rows="5" id="alamat_volunteer" name="alamat_volunteer"  cols="10" rows="2" required></textarea>
                      </div>
                  </div>
                        
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary" value="upload">Submit</button>
                    </div>
                </form>
            </div>
        </div>  
    
      </div>
    </section>
  </div>

  {{-- batasan size images --}}
<script type="text/javascript">
  $(document).ready(function() {
    maxFileSize = 10 * 1024 * 1024 / 2; // 10 mb
    imageSize = maxFileSize / 10;

    $('#images').change(function() {
      fileSize = this.files[0].size;

      if (fileSize > imageSize) {
        this.setCustomValidity("You can upload only files under 1 MB");
        this.reportValidity();
      } else {
        this.setCustomValidity("");
      }
    });
  });
</script>
