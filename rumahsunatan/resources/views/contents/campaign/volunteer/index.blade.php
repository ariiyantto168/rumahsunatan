<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Volunteer Campaign</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Campaign</li>
              <li class="breadcrumb-item active"><a href="{{ url('campaign/volunteer') }}">Volunteer</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Index</h3>
                <div class="float-right"><i class="fas fa-plus size:2x"></i> <a href="{{ url('campaign/volunteer/create-new') }}">Create New</a></div>

            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Volunteer</th>
                          <th>Nama Pengguna</th>
                          <th>Tipe</th>
                          <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($volunteer as $idx => $vol)
                      <tr>
                          <td>{{ $idx+1 }}</td>
                          <td>{{ $vol->nama_volunteer }}</td>
                          <td>{{ $vol->pengguna->nama }}</td>
                          <td>{{ $vol->tipe }}</td>
                        <td>
                            <a href="{{ url('campaign/volunteer/update/'. $vol->id_volunteer) }}"><i class="fas fa-edit" title="Update New Class"></i></a>
                        </td>  
                      </tr>
                    @endforeach
                  </tbody>                    
                </table>
            </div>
        </div>  
    
      </div>
    </section>
  </div>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
  });
</script>
