<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Campaign</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="{{ url('campaign') }}">Campaign</a></li>
              <li class="breadcrumb-item active">Create-new</li>
            </ol>
          </div><!-- /.col -->      
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create</h3>
            </div>
            <div class="card-body">
              @include('contents.allmessage')
                <form role="form" class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ url('campaign/create-new') }}">  
                  @csrf
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Campaign</label>
                        <div class="col-sm-5">
                          <input type="text" name="nama" placeholder="Example: Tekhnologi" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Volunteer Campaign</label>
                        <div class="col-sm-5">
                        <select name="id_volunteer" id="select2" class="form-control" required>
                              <option value=""> -- select volunteer -- </option>
                              @foreach ($volunteer as $vol)
                                <option value="{{$vol->id_volunteer}}">{{$vol->nama_volunteer}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Campaign</label>
                        <div class="col-sm-5">
                        <select name="id_jenis" id="select2" class="form-control" required>
                              <option value=""> -- select jeni -- </option>
                              @foreach ($jenis as $jen)
                                <option value="{{$jen->id_jenis}}">{{$jen->nama}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Kategori Campaign</label>
                        <div class="col-sm-5">
                        <select name="id_kategori" id="select2" class="form-control" required>
                              <option value=""> -- select kategori -- </option>
                              @foreach ($kategori as $kat)
                                <option value="{{$kat->id_kategori}}">{{$kat->nama}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Program Campaign</label>
                        <div class="col-sm-5">
                        <select name="id_program" id="select2" class="form-control" required>
                              <option value=""> -- select program -- </option>
                              @foreach ($program as $pro)
                                <option value="{{$pro->id_program}}">{{$pro->nama}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Total Biaya</label>
                        <div class="col-sm-5">
                          <input type="number" name="total_biaya" placeholder="Example: 8000000" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Tanggal berjalan kegiatan</label>
                        <div class="col-sm-5">
                        <input class="form-control" id="date" name="tgl_berjalan_kegiatan" placeholder="YYYY-MM-DD" type="text"/>
                        </div>
                    </div>
                        
                    <div class="form-group">
                      <label for="exampleInputEmail1">Tanggal Berahkir kegiatan</label>
                        <div class="col-sm-5">
                        <input class="form-control" id="date" name="exp_date" placeholder="YYYY-MM-DD" type="text"/>
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="name_materi" class="col-sm-2 col-form-label">Description Campaign</label>
                      <div class="col-sm-5">
                        <textarea class="form-control" rows="5" id="description" name="deskripsi"  cols="10" rows="2" required></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="name_materi" class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-5">
                        <input type="checkbox" name="status" checked>
                      </div>
                    </div>
                        
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary" value="upload">Submit</button>
                    </div>
                </form>
            </div>
        </div>  
    
      </div>
    </section>
  </div>

  {{-- batasan size images --}}
<script type="text/javascript">
  $(document).ready(function() {
    maxFileSize = 10 * 1024 * 1024 / 2; // 10 mb
    imageSize = maxFileSize / 10;

    $('#images').change(function() {
      fileSize = this.files[0].size;

      if (fileSize > imageSize) {
        this.setCustomValidity("You can upload only files under 1 MB");
        this.reportValidity();
      } else {
        this.setCustomValidity("");
      }
    });
  });
</script>
