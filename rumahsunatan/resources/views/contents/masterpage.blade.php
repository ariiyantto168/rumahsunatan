<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>rumahsunatan.id</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" >
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/css/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/css/jquery.vmap.min.js">
  <!-- Theme style -->
  <link rel="stylesheet" href="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/overlayscrollbars/1.13.1/css/OverlayScrollbars.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/css/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/css/summernote-bs4.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/css/responsive.bootstrap4.min.css">
  {{--  select2  --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- jQuery -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/jquery-ui.min.js"></script>
{{-- date --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

<style>
    .textbox{
        background-color: #6c757d !important;
      }
    .blue{
      background-color: #020114 !important;
    }
</style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link btn" data-toggle="modal" data-target="#modal-logout" role="button">
          <i class="fas fa-sign-out-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4" style="background-color: #152ca3 !important">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="nav nav-pills nav-sidebar flex-column" style="color:#F5F5F5;" data-widget="treeview" role="menu" data-accordion="false">{{Auth::guard('pengguna')->user()->nama}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item " >
            <a href="{{ url('/') }}" class="nav-link" id="menu_home" style="color:#F5F5F5;">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                Home
              </p>
            </a>
          </li>
          <li class="nav-item " >
            <a href="{{ url('data/donasi') }}" class="nav-link" id="menu_donasi" style="color:#F5F5F5;">
                <i class="nav-icon fab fa-hive"></i>
                <p>
                Donasi
              </p>
            </a>
          </li>  
          <li class="nav-item">
            <a href="#" class="nav-link" id="menu_campaign" style="color:#F5F5F5;">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Campaign
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: block;">
              <li class="nav-item" style="color:#F5F5F5;">
                <a href="{{ url('campaign') }}" class="nav-link" id="submenu_campaign" style="color:#F5F5F5;">
                  <i class="fas fa-users nav-icon"></i>
                  <p>Campaign</p>
                </a>
              </li>
              <li class="nav-item" style="color:#F5F5F5;">
                <a href="{{ url('campaign/kategori') }}" class="nav-link" id="submenu_kategori" style="color:#F5F5F5;">
                  <i class="fas fa-cubes nav-icon"></i>
                  <p>Kategori Campaign</p>
                </a>
              </li>
              <li class="nav-item" style="color:#F5F5F5;">
                <a href="{{ url('campaign/jenis') }}" class="nav-link" id="submenu_jenis" style="color:#F5F5F5;">
                  <i class="far fa-file nav-icon"></i>
                  <p>Jenis Campaign</p>
                </a>
              </li>
              <li class="nav-item" style="color:#F5F5F5;">
                <a href="{{ url('campaign/jenisbiaya') }}" class="nav-link" id="submenu_jenisbiaya" style="color:#F5F5F5;">
                  <i class="fas fa-tags nav-icon"></i>
                  <p>Jenis Biaya</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('campaign/program') }}" class="nav-link" id="submenu_program" style="color:#F5F5F5;">
                  <i class="fas fa-project-diagram nav-icon"></i>
                  <p>Program Campaign</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('campaign/volunteer') }}" class="nav-link" id="submenu_volunteer" style="color:#F5F5F5;">
                  <i class="fas fa-user nav-icon"></i>
                  <p>Volunteer</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->

    {!! $pagecontent !!}


  <!-- /.content-wrapper -->
  
  <footer class="main-footer" style="color:#000000;">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2021 - 2022 <a href="#"> rumahsunatan.id</a>.</strong>
    <b style="color:#000000;">All rights reserved.</b>
  </footer>
  {{--  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.5
    </div>
  </footer>  --}}

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Modal -->
<div class="modal fade" id="modal-logout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure logout this application ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="{{ url('logout') }}" class="btn btn-danger">Logout</a>
      </div>
    </div>
  </div>
</div>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/bootstrap.bundle.min.js"></script>
<!-- Sparkline -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/sparkline.js"></script>
<!-- JQVMap -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/jquery.vmap.min.js"></script>
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/moment.min.js"></script>
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/daterangepicker.js"></script>
<!-- DataTables -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/jquery.dataTables.min.js"></script>
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/dataTables.bootstrap4.min.js"></script>
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/dataTables.responsive.min.js"></script>
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/responsive.bootstrap4.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<!-- {{--  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>  --}} -->
<!-- Summernote -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="https://ekselen-storage.s3-ap-southeast-1.amazonaws.com/ADMINLTE/js/demo.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>

<!-- DATE -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Menambahakan Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]');
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'mm/dd/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true,
		})
	})
</script>
<!-- DATE -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <script>
    //Initialize Select2 Elements
      $('.select2').select2()
  
      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })
      $(function () {
        $("#example1").DataTable({
          "responsive": true,
          "autoWidth": false,
        });
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
  
      $('#menu_{{$menu}}').addClass('active textbox');
      $('#submenu_{{$submenu}}').addClass('active textbox');
  </script>
   <script>
    //  date range
    $(function () {
      $('.reservation').daterangepicker({
        locale: {
          format: 'YYYY-MM-DD'
        }
      })
    })

    //Date picker
 $('.datepicker').datepicker({
   autoclose: true,
   dateFormat: 'yyyy-mm-dd'
 });

 // date time 
 $('.reservationtime').daterangepicker({
  timePicker: true,
  timePickerIncrement: 30,
  locale: {
    format: 'MM/DD/YYYY hh:mm A'
  }
  })
  </script>
  
</body>
</html>
