<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Donasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Donasi Campaign</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Donasi Campaign</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
              <div class="row">
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Volunteer</span>
                      <span class="info-box-number text-center text-muted mb-0">{{$campaign->volunteer->nama_volunteer}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Program Campaign</span>
                      <span class="info-box-number text-center text-muted mb-0">{{$campaign->program->nama}}</span>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-4">
                  <div class="info-box bg-light">
                    <div class="info-box-content">
                      <span class="info-box-text text-center text-muted">Kategori Campaign</span>
                      <span class="info-box-number text-center text-muted mb-0">{{$campaign->kategori->nama}}<span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <h4>Class Activity</h4>
                    <div class="post">
                    </div>

                    <div class="post clearfix">
                        <div class="box-body table-responsive">
                            <table id="" class="table table-bordered table-striped">
                              <tbody>
                                <tr>
                                    <td width="15%">Class</td>
                                    <td width="2%">:</td>
                                    <td>{{$campaign->nama_campaign}}</td>
                                </tr>
                                <tr>
                                    <td width="15%">Volunteer</td>
                                    <td width="2%">:</td>
                                    <td>{{$campaign->volunteer->nama_volunteer}}</td>
                                </tr>
                                <tr>
                                    <td width="15%">Total Biaya</td>
                                    <td width="2%">:</td>
                                    <td>{{$campaign->total_biaya}}</td>
                                </tr>
                                <tr>
                                    <td width="15%">Start Date</td>
                                    <td width="2%">:</td>
                                    <td>{{$campaign->tgl_berjalan_kegiatan}}</td>
                                </tr>
                                <tr>
                                    <td width="15%">End Date</td>
                                    <td width="2%">:</td>
                                    <td>{{$campaign->exp_date}}</td>
                                </tr>
                                <tr>
                                    <td width="15%">Statu</td>
                                    <td width="2%">:</td>
                                    <td>
                                        @if($campaign->status)
                                        <span class="btn btn-block btn-success btn-sm" style="width:70px;">Active</span>
                                        @else
                                        <span class="btn btn-block btn-danger btn-sm" style="width:70px;">Inactive</span>
                                        @endif
                                    </td>
                                </tr>
                                
                                </tr>
                              </tbody>
                            </table>
                          </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
              <h3 class="text-primary"><i class="fas fa-paint-brush"></i>Deskripsi</h3>
              <p class="text-muted">{{$campaign->deskripsi_campaign}}</p>
              <br>
              <div class="text-muted">
                <p class="text-sm">Client Company
                  <b class="d-block">Rumah Sunatan</b>
                </p>
                <p class="text-sm">Project Leader
                  <b class="d-block">{{$campaign->volunteer->nama_volunteer}}</b>
                </p>
              </div>

              <h5 class="mt-5 text-muted">Project files</h5>
              <ul class="list-unstyled">
                <li>
                  <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Functional-requirements.docx</a>
                </li>
                <li>
                  <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i> UAT.pdf</a>
                </li>
                <li>
                  <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-envelope"></i> Email-from-flatbal.mln</a>
                </li>
                <li>
                  <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-image "></i> Logo.png</a>
                </li>
                <li>
                  <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i> Contract-10_12_2014.docx</a>
                </li>
              </ul>
              <div class="text-center mt-5 mb-3">
                <a href="#" data-toggle="modal" data-target="#donasi" class="btn btn-sm btn-primary">Donasi</a>
                <a href="#" class="btn btn-sm btn-warning">Back</a>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>


  <div class="modal fade" id="donasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form action="{{ url('donasi/pengguna/'.$campaign->id_campaign.'/add-donasi') }}" role="form" method="post">
            @csrf
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h5 class="modal-title" id="exampleModalCenterTitle">Donasi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                            <div class="table-responsive">
                                <table class="table" id="table_marketplace">
                                    <tbody>
                                        <tr>
                                            <td width="8%">Nama Donatur</td>
                                            <td><input type="text" class="form-control"  name="nama_donatur" id="nama_donatur" required></td>
                                        </tr>
                                        <tr>
                                            <td width="8%">Jenis Biaya</td>
                                            <td>
                                            <select name="id_jenis_biaya" id="select2" class="form-control" required>
                                              <option value=""> -- select jenis biaya -- </option>
                                              @foreach ($jenisbiaya as $jenis)
                                                <option value="{{$jenis->id_jenis_biaya}}">{{$jenis->nama}}</option>
                                              @endforeach
                                            </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="8%">email</td>
                                            <td><input type="email" class="form-control"  name="email" id="email" required></td>
                                        </tr>
                                        <tr>
                                            <td width="8%">telepon</td>
                                            <td><input type="number" class="form-control"  name="telp" id="telp" required></td>
                                        </tr>
                                        <tr>
                                            <td width="8%">Nominal</td>
                                            <td><input type="number" class="form-control"  name="nominal" id="nominal" required></td>
                                        </tr>
                                        <tr>
                                            <td width="8%">Pembayaran</td>
                                            <td>
                                              <select name="by" id="select2" class="form-control" required>
                                                <option value="online">Online</option>
                                                <option value="offline">Offline</option>
                                              </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="8%">Tipe</td>
                                            <td>
                                              <select name="tipe" id="select2" class="form-control" required>
                                                <option value="penggalangan">Penggalangan</option>
                                                <option value="Pencairan">Pencairan</option>
                                              </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="8%">Doa Dan Harapan</td>
                                            <td>
                                              <textarea class="form-control" rows="5" id="doa_harapan" name="doa_harapan"  cols="10" rows="2" required></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            <input type="hidden" value="2" id="index_marketplace">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
          <input type="submit" class="btn btn-primary" value="Save">
        </div>
      </div>
    </form>
      </div>
    </div>
