<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolunteerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteer', function (Blueprint $table) {
            $table->increments('id_volunteer');
            $table->integer('id_pengguna');
            $table->string('nama_volunteer');
            $table->text('alamat_volunteer');
            $table->enum('tipe',['volunteer','pribadi']);
            $table->string('npwp');
            $table->string('telp');
            $table->date('tgl_buat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteer');
    }
}
