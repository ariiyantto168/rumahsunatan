<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->increments('id_campaign');
            $table->integer('id_pengguna');
            $table->integer('id_jenis');
            $table->integer('id_kategori');
            $table->integer('id_program');
            $table->integer('id_volunteer');
            $table->string('nama_campaign');
            $table->string('total_biaya');
            $table->text('deskripsi_campaign');
            $table->date('tgl_berjalan_kegiatan');
            $table->date('exp_date');
            $table->date('tgl_buat');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}
