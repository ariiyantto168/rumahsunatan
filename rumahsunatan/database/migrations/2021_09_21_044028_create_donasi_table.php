<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donasi', function (Blueprint $table) {
            $table->increments('id_donasi');
            $table->integer('id_campaign');
            $table->integer('id_pengguna');
            $table->integer('id_jenis_biaya');
            $table->string('nama_donatur');
            $table->string('email');
            $table->string('telp');
            $table->text('doa_harapan');
            $table->string('nominal');
            $table->date('tgl_donasi');
            $table->enum('by',['online','offline']);
            $table->enum('tipe',['penggalangan','pencairan']);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donasi');
    }
}
