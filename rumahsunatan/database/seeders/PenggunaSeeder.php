<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pengguna;
use App\Models\Role;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PenggunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dbrole = [
                [
                    'role' => 'admin',
                    'is_active' => true,
                    'deskripsi_role' => 'akses admin'
                ],
                [
                    'role' => 'pengguna',
                    'is_active' => true,
                    'deskripsi_role' => 'akses pengguna'
                ]
            ];
        DB::table('role')->insert($dbrole);

        $dbusers = [
            [
               'id_role' => 1,
               'nama' => 'admin',
               'email' => 'admin@rumahsunatan.id',
               'no_wa' => '08118872339',
               'is_active' => true,
               'password' =>  Hash::make('admin123'),
            ],
            [
                'id_role' => 2,
                'nama' => 'pengguna',
                'email' => 'pengguna@rumahsunatan.id',
                'no_wa' => '08118872339',
                'is_active' => true,
                'password' =>  Hash::make('pengguna123'),
             ]
       ];

       DB::table('pengguna')->insert($dbusers);
    }
}
