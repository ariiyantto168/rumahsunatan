<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use App\Models\Campaign\Jenis;
use App\Models\Campaign\Kategori;
use App\Models\Campaign\Program;
use App\Models\Campaign\Volunteer;
use App\Models\Campaign\Jenisbiaya;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CampaignmasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dbjenis = [
            [
               'nama' => 'sosial',
               'deskripsi' => 'mendeskripsikan tentang sosial',
               'is_active' => true,
            ],
            [
                'nama' => 'masyarakat',
                'deskripsi' => 'mendeskripsikan tentang masyarakat',
                'is_active' => true,
             ],
             [
                'nama' => 'peduli covid',
                'deskripsi' => 'mendeskripsikan tentang membantu korban covid',
                'is_active' => true,
             ]
       ];

       DB::table('jenis')->insert($dbjenis);


       $dbjenisbiaya = [
         [
            'nama' => 'cash',
            'deskripsi' => 'mendeskripsikan tentang uang cash',
            'is_active' => true,
         ],
         [
             'nama' => 'transfer',
             'deskripsi' => 'mendeskripsikan tentang pembayaran transfer',
             'is_active' => true,
          ],
          [
             'nama' => 'tunai',
             'deskripsi' => 'mendeskripsikan pembayaran tunai',
             'is_active' => true,
          ]
      ];

      DB::table('jenis_biaya')->insert($dbjenisbiaya);

       $dbkategori = [
         [
            'nama' => 'politik',
            'deskripsi' => 'mendeskripsikan tentang politik',
            'is_active' => true,
         ],
         [
             'nama' => 'IT',
             'deskripsi' => 'mendeskripsikan tentang IT',
             'is_active' => true,
          ],
          [
             'nama' => 'Management',
             'deskripsi' => 'mendeskripsikan tentang Management',
             'is_active' => true,
          ]
      ];

      DB::table('kategori')->insert($dbkategori);
      


      $dbprogram = [
         [
            'nama' => '17 Agustusan',
            'deskripsi' => 'mendeskripsikan tentang 17 Agustusan',
            'is_active' => true,
         ],
         [
             'nama' => 'Hari pendidikan',
             'deskripsi' => 'mendeskripsikan tentang IT',
             'is_active' => true,
          ],
          [
             'nama' => 'Tahun Baru',
             'deskripsi' => 'mendeskripsikan tentang Hari pendidikan',
             'is_active' => true,
          ]
      ];

      DB::table('program')->insert($dbprogram);
      
      $dbVolunteer = [
         [
            'id_pengguna' => 1,
            'nama_volunteer' => 'ari',
            'tipe' => 'pribadi',
            'npwp' => '1111108108',
            'telp' => '081341557945',
            'alamat_volunteer' => 'jakarta barat',
            'tgl_buat' => date('Y-m-d'),
         ],
         [
            'id_pengguna' => 1,
            'nama_volunteer' => 'yanto',
            'tipe' => 'pribadi',
            'npwp' => '1111108108',
            'telp' => '081341557945',
            'alamat_volunteer' => 'jakarta barat',
            'tgl_buat' => date('Y-m-d'),
         ],
         [
            'id_pengguna' => 1,
            'nama_volunteer' => 'ariyanto',
            'tipe' => 'volunteer',
            'npwp' => '1111108108',
            'telp' => '081341557945',
            'alamat_volunteer' => 'head office',
            'tgl_buat' => date('Y-m-d'),
         ]

         
      ];

      DB::table('volunteer')->insert($dbVolunteer);

   }

}
