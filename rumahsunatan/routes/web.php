<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Campaign\KategoriController;
use App\Http\Controllers\Campaign\JenisController;
use App\Http\Controllers\Campaign\ProgramController;
use App\Http\Controllers\Campaign\CampaignController;
use App\Http\Controllers\Campaign\VolunteerController;
use App\Http\Controllers\Campaign\JenisbiayaController;
use App\Http\Controllers\Donasi\DonasicampaignController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', [AuthController::class, 'login']);
Route::post('login-do', [AuthController::class, 'do_login']);
Route::get('logout', [AuthController::class, 'logout']);

Route::get('admin', [HomeController::class, 'admin'])->middleware('check.auth');
Route::get('pengguna', [HomeController::class, 'pengguna'])->middleware('check.users');

// campaign kategori
Route::get('campaign/kategori', [KategoriController::class, 'index'])->middleware('check.auth');
Route::get('campaign/kategori/create-new', [KategoriController::class, 'create_page'])->middleware('check.auth');
Route::post('campaign/kategori/create-new', [KategoriController::class, 'create_save'])->middleware('check.auth');
Route::get('campaign/kategori/update/{kategori}', [KategoriController::class, 'update_page'])->middleware('check.auth');
Route::post('campaign/kategori/update/{kategori}', [KategoriController::class, 'update_save'])->middleware('check.auth');

// campaign jenis
Route::get('campaign/jenis', [JenisController::class, 'index'])->middleware('check.auth');
Route::get('campaign/jenis/create-new', [JenisController::class, 'create_page'])->middleware('check.auth');
Route::post('campaign/jenis/create-new', [JenisController::class, 'create_save'])->middleware('check.auth');
Route::get('campaign/jenis/update/{jenis}', [JenisController::class, 'update_page'])->middleware('check.auth');
Route::post('campaign/jenis/update/{jenis}', [JenisController::class, 'update_save'])->middleware('check.auth');

// campaign program
Route::get('campaign/program', [ProgramController::class, 'index'])->middleware('check.auth');
Route::get('campaign/program/create-new', [ProgramController::class, 'create_page'])->middleware('check.auth');
Route::post('campaign/program/create-new', [ProgramController::class, 'create_save'])->middleware('check.auth');
Route::get('campaign/program/update/{program}', [ProgramController::class, 'update_page'])->middleware('check.auth');
Route::post('campaign/program/update/{program}', [ProgramController::class, 'update_save'])->middleware('check.auth');

// volunteer
Route::get('campaign/volunteer', [VolunteerController::class, 'index'])->middleware('check.auth');
Route::get('campaign/volunteer/create-new', [VolunteerController::class, 'create_page'])->middleware('check.auth');
Route::post('campaign/volunteer/create-new', [VolunteerController::class, 'create_save'])->middleware('check.auth');
// Route::get('campaign/jenis/update/{jenis}', [JenisController::class, 'update_page'])->middleware('check.auth');
// Route::post('campaign/jenis/update/{jenis}', [JenisController::class, 'update_save'])->middleware('check.auth');


// campaign 
Route::get('campaign', [CampaignController::class, 'index'])->middleware('check.auth');
Route::get('campaign/create-new', [CampaignController::class, 'create_page'])->middleware('check.auth');
Route::post('campaign/create-new', [CampaignController::class, 'create_save'])->middleware('check.auth');
Route::get('campaign/update/{campaign}', [CampaignController::class, 'update_page'])->middleware('check.auth');
Route::post('campaign/update/{campaign}', [CampaignController::class, 'update_save'])->middleware('check.auth');
Route::get('data/donasi', [CampaignController::class, 'history_donasi'])->middleware('check.auth');

// jenis biaya
Route::get('campaign/jenisbiaya', [JenisbiayaController::class, 'index'])->middleware('check.auth');
Route::get('campaign/jenisbiaya/create-new', [JenisbiayaController::class, 'create_page'])->middleware('check.auth');
Route::post('campaign/jenisbiaya/create-new', [JenisbiayaController::class, 'create_save'])->middleware('check.auth');
Route::get('campaign/jenisbiaya/update/{jenis}', [JenisbiayaController::class, 'update_page'])->middleware('check.auth');
Route::post('campaign/jenisbiaya/update/{jenis}', [JenisbiayaController::class, 'update_save'])->middleware('check.auth');

// donasi campaign
Route::get('donasi/pengguna', [DonasicampaignController::class, 'index'])->middleware('check.users');
Route::get('history-donasi', [DonasicampaignController::class, 'history_donasi'])->middleware('check.users');
Route::get('donasi/pengguna/{campaign}', [DonasicampaignController::class, 'donasi_page'])->middleware('check.users');
Route::post('donasi/pengguna/{campaign}/add-donasi', [DonasicampaignController::class, 'donasi_save'])->middleware('check.users');