public function do_login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');
        
        if (Auth::attempt($credentials)) {
            $datauser = User::where('email',$request->email)->first();
            $datauser->last_login = date('Y-m-d H:i:s');
            $datauser->save();
            // user session 
            $alldata = [
                'idusers' => $datauser->idusers,
                'name' => $datauser->name,
                'email' => $datauser->email,
                'role' => $datauser->role,
            ];
            $role = $alldata['role'];
            if($role === 'u'){
                return redirect()->back()->with('status_error','Not permission login');
            }

            // privileges session
            $privileges = $this->get_user_privileges($datauser->idusers);
            // store session
            Session::put('users',$alldata);
            Session::put('privileges',$privileges);

            return redirect('/');
        }else{
            return redirect('login')->with('status_warning','email or password something went wrong');
        }


    }

                // return Auth::guard('user')->user()->name;
                     <!-- <a href="#" class="nav nav-pills nav-sidebar flex-column" style="color:#F5F5F5;" data-widget="treeview" role="menu" data-accordion="false">{{Auth::guard('user')->user()->name}}</a> -->

                     $dbusers = [
            [
               'name' => 'admin',
               'role' => 'admin',
               'email' => 'admin@kitakompeten.id',
               'no_wa' => '08118872339',
               'is_active' => true,
               'password' =>  Hash::make('admin123'),
            ],
            [
                'name' => 'Pengguna',
                'role' => 'pengguna',
                'email' => 'pengguna@kitakompeten.id',
                'no_wa' => '08118872339',
                'address' => 'jakarta',
                'is_active' => true,
                'password' =>  Hash::make('pengguna123'),
             ]
       ];