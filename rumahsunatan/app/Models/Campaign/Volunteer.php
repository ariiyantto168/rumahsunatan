<?php

namespace App\Models\Campaign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = 'volunteer';
    protected $primaryKey = 'id_volunteer';

    protected $fillable = [
    ];

    public function pengguna()
    {
        return $this->belongsTo('App\Models\Pengguna', 'id_pengguna');
    }
}
