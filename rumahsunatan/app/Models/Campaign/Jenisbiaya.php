<?php

namespace App\Models\Campaign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenisbiaya extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = 'jenis_biaya';
    protected $primaryKey = 'id_jenis_biaya';

    protected $fillable = [
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];
}
