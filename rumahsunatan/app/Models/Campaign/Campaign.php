<?php

namespace App\Models\Campaign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = 'campaign';
    protected $primaryKey = 'id_campaign';

    protected $fillable = [
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    public function jenis()
    {
        return $this->belongsTo('App\Models\Campaign\Jenis', 'id_jenis');
    }

    public function jenisbiaya()
    {
        return $this->belongsTo('App\Models\Campaign\Jenisbiaya', 'id_jenis_biaya');
    }

    public function program()
    {
        return $this->belongsTo('App\Models\Campaign\Program', 'id_program');
    }

    public function kategori()
    {
        return $this->belongsTo('App\Models\Campaign\Kategori', 'id_kategori');
    }

    public function volunteer()
    {
        return $this->belongsTo('App\Models\Campaign\Volunteer', 'id_volunteer');
    }

}
