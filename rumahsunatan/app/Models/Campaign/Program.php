<?php

namespace App\Models\Campaign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = 'program';
    protected $primaryKey = 'id_program';

    protected $fillable = [
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];
}
