<?php

namespace App\Models\Campaign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donasi extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = 'donasi';
    protected $primaryKey = 'id_donasi';

    protected $fillable = [
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign\Campaign', 'id_campaign');
    }

    public function pengguna()
    {
        return $this->belongsTo('App\Models\Pengguna', 'id_pengguna');
    }

    public function jenisbiaya()
    {
        return $this->belongsTo('App\Models\Campaign\Jenisbiaya', 'id_jenis_biaya');
    }
}
