<?php

namespace App\Models\Campaign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = 'kategori';
    protected $primaryKey = 'id_kategori';

    protected $fillable = [
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];



}
