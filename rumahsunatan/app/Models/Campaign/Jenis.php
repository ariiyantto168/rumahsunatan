<?php

namespace App\Models\Campaign;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = 'jenis';
    protected $primaryKey = 'id_jenis';

    protected $fillable = [
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];
}
