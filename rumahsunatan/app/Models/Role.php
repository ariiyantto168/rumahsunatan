<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $dates = ['deleted_at'];

    protected $table = 'role';
    protected $primaryKey = 'id_role';

    protected $fillable = [
    ];

    protected $casts = [
        'is_active' => 'boolean'
    ];

    public function pengguna()
    {
        return $this->belongsTo('App\Models\Pengguna', 'id_role');
    }
}
