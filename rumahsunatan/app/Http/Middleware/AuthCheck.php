<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Pengguna;

class AuthCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $datauser = Auth::guard('pengguna')->user();
        $pengguna = Pengguna::with(['role'])
                                ->where('id_pengguna', $datauser->id_pengguna)
                                ->first();

        if(!$request->session()->exists('pengguna')) {
            return redirect('login')->with('status_warning', 'Please Login!');
        }elseif ($pengguna->role->role != "admin") {
            Session::flush();
            Auth::logout();
            return redirect('login');
        }

        return $next($request);

    }
}
