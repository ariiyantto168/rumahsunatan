<?php

namespace App\Http\Controllers\Donasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign\Campaign;
use App\Models\Campaign\Donasi;
use App\Models\Campaign\Jenisbiaya;
use Illuminate\Support\Facades\Auth;

class DonasicampaignController extends Controller
{
    public function index()
    {
        $contents = [
            'campaign' => Campaign::all(),
        ];
        
        $pagecontent = view('contents.donasi.campaign.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'donasi campaign',
            'menu' => 'home',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.penggunapage', $pagemain);

    }

    public function donasi_page(Campaign $campaign)
    {
        $donasi = Campaign::with([
                            'jenis',
                            'kategori',
                            'program',
                            'volunteer',
                        ])
                        ->where('id_campaign', $campaign->id_campaign)
                        ->first();

        $contents = [
            'campaign' => $donasi,
            'jenisbiaya' => Jenisbiaya::all(),
        ];

        $pagecontent = view('contents.donasi.donasi.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'donasi campaign',
            'menu' => 'home',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.penggunapage', $pagemain);
    }

    public function donasi_save(Campaign $campaign,Request $request)
    {
        $donasiCampaign = new Donasi;
        $donasiCampaign->id_campaign = $campaign->id_campaign;
        $donasiCampaign->id_jenis_biaya = $request->id_jenis_biaya;
        $donasiCampaign->id_pengguna = Auth::guard('pengguna')->user()->id_pengguna;
        $donasiCampaign->nama_donatur = $request->nama_donatur;
        $donasiCampaign->email = $request->email;
        $donasiCampaign->telp = $request->telp;
        $donasiCampaign->doa_harapan = $request->doa_harapan;
        $donasiCampaign->nominal = $request->nominal;
        $donasiCampaign->tgl_donasi = $request->tgl_donasi;
        $donasiCampaign->by = $request->by;
        $donasiCampaign->tipe = $request->tipe;
        $donasiCampaign->status = true;
        $donasiCampaign->tgl_donasi = date('Y-m-d');
        $donasiCampaign->save();

        return redirect('donasi/pengguna/'. $campaign->id_campaign)->with('status_success','Successfuly Add Donasi');

    }

    public function history_donasi()
    {
        $donasi = Donasi::with(['jenisbiaya'])
                        ->where('id_pengguna', Auth::guard('pengguna')->user()->id_pengguna)
                        ->get();
        $contents = [
            'donasi' => $donasi,
        ];

        
        $pagecontent = view('contents.donasi.campaign.history', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'donasi campaign',
            'menu' => 'home',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.penggunapage', $pagemain);
    }
}
