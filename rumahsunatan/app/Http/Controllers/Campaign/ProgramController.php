<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign\Program;

class ProgramController extends Controller
{
    public function index()
    {
        $contents = [
            'program' => Program::all(),
        ];

        
        $pagecontent = view('contents.campaign.program.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Program',
            'menu' => 'campaign',
            'submenu' => 'program',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_page()
    {
        $contents = [
        ];

        $pagecontent = view('contents.campaign.program.create', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'program',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_save(Request $request)
    {
        $active = FALSE;
        if($request->has('is_active')) {
            $active = TRUE;
        }

        $saveCampaign = new Program;
        $saveCampaign->nama = $request->nama;
        $saveCampaign->deskripsi = $request->deskripsi;
        $saveCampaign->is_active = $active;
        $saveCampaign->save();

        return redirect('campaign/program')->with('status_success','Successfuly Add Program');

    }

    public function update_page(Program $program)
    {
        $contents = [
            'program' => Program::where('id_program', $program->id_program)->first(),
        ];


        $pagecontent = view('contents.campaign.program.update', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'program',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);
    }

    public function update_save(Program $program ,Request $request)
    {
        $active = FALSE;
        if($request->has('is_active')) {
            $active = TRUE;
        }

        $saveCampaign = Program::where('id_program', $program->id_program)->first();
        $saveCampaign->nama = $request->nama;
        $saveCampaign->deskripsi = $request->deskripsi;
        $saveCampaign->is_active = $active;
        $saveCampaign->save();

        return redirect('campaign/program')->with('status_success','Successfuly Add Kategori');

    }
}
