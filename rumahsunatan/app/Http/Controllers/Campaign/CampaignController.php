<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign\Jenis;
use App\Models\Campaign\Donasi;
use App\Models\Campaign\Kategori;
use App\Models\Campaign\Program;
use App\Models\Campaign\Campaign;
use App\Models\Campaign\Volunteer;
use Illuminate\Support\Facades\Auth;

class CampaignController extends Controller
{
    public function index()
    {
        
        $campaign = Campaign::with([
                    'jenis',
                    'kategori',
                    'program',
                    'volunteer'
                ])->get();

        $contents = [
            'campaign' => $campaign,
        ];

        
        $pagecontent = view('contents.campaign.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'campaign',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_page()
    {
        $contents = [
            'kategori' => Kategori::all(),
            'jenis' => Jenis::all(),
            'program' => Program::all(),
            'volunteer' => Volunteer::all(),
        ];

        $pagecontent = view('contents.campaign.create', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign',
            'menu' => 'campaign',
            'submenu' => 'campaign',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_save(Request $request)
    {
        $active = FALSE;
        if($request->has('status')) {
            $active = TRUE;
        }

        $campaign = new Campaign;
        $campaign->nama_campaign = $request->nama;
        $campaign->id_pengguna = Auth::guard('pengguna')->user()->id_pengguna;
        $campaign->id_jenis = $request->id_jenis;
        $campaign->id_volunteer = $request->id_volunteer;
        $campaign->id_kategori = $request->id_kategori;
        $campaign->id_program = $request->id_program;
        $campaign->total_biaya = $request->total_biaya;
        $campaign->tgl_berjalan_kegiatan = $request->tgl_berjalan_kegiatan;
        $campaign->exp_date = $request->exp_date;
        $campaign->tgl_buat = date('Y-m-d');
        $campaign->deskripsi_campaign = $request->deskripsi;
        $campaign->status = $active;
        $campaign->save();
        return redirect('campaign')->with('status_success','Successfuly Add Kategori');

    }

    public function update_page(Campaign $campaign)
    {
        $campaign = Campaign::with([
                            'jenis',
                            'kategori',
                            'program',
                            'volunteer'
                            ])
                            ->where('id_campaign', $campaign->id_campaign)
                            ->first();
        
        $contents = [
            'campaign' => $campaign,
            'kategori' => Kategori::all(),
            'jenis' => Jenis::all(),
            'program' => Program::all(),
            'volunteer' => Volunteer::all(),
        ];

        $pagecontent = view('contents.campaign.update', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign',
            'menu' => 'campaign',
            'submenu' => 'campaign',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);
    }

    public function update_save(Campaign $campaign,Request $request)
    {
        $active = FALSE;
        if($request->has('status')) {
            $active = TRUE;
        }

        $campaign = Campaign::where('id_campaign', $campaign->id_campaign)->first();
        $campaign->nama_campaign = $request->nama;
        $campaign->id_pengguna = Auth::guard('pengguna')->user()->id_pengguna;
        $campaign->id_jenis = $request->id_jenis;
        $campaign->id_volunteer = $request->id_volunteer;
        $campaign->id_kategori = $request->id_kategori;
        $campaign->id_program = $request->id_program;
        $campaign->total_biaya = $request->total_biaya;
        $campaign->tgl_berjalan_kegiatan = $request->tgl_berjalan_kegiatan;
        $campaign->exp_date = $request->exp_date;
        $campaign->tgl_buat = date('Y-m-d');
        $campaign->deskripsi_campaign = $request->deskripsi;
        $campaign->status = $active;
        $campaign->save();
        return redirect('campaign')->with('status_success','Successfuly Add Kategori');
    }

    public function history_donasi()
    {
        $contents = [
            'donasi' => Donasi::with(['jenisbiaya'])->get(),
        ];

        $pagecontent = view('contents.campaign.donasi', $contents);

        //masterpage
        $pagemain = array(
            'title' => 'Campaign Donasi',
            'menu' => 'donasi',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);
    }
}
