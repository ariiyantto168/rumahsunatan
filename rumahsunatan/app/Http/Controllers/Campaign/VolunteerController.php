<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign\Volunteer;
use Illuminate\Support\Facades\Auth;
use App\Models\Pengguna;

class VolunteerController extends Controller
{
    public function index()
    {
        $contents = [
            'volunteer' => Volunteer::with(['pengguna'])->get(),
        ];
        
        $pagecontent = view('contents.campaign.volunteer.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Volunteer',
            'menu' => 'campaign',
            'submenu' => 'volunteer',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_page()
    {
        $contents = [
        ];

        $pagecontent = view('contents.campaign.volunteer.create', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Volunteer',
            'menu' => 'campaign',
            'submenu' => 'volunteer',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_save(Request $request)
    {

        $saveCampaign = new Volunteer;
        $saveCampaign->id_pengguna = Auth::guard('pengguna')->user()->id_pengguna;
        $saveCampaign->nama_volunteer = $request->nama_volunteer;
        $saveCampaign->tipe = $request->tipe;
        $saveCampaign->npwp = $request->npwp;
        $saveCampaign->telp = $request->tlp;
        $saveCampaign->alamat_volunteer = $request->alamat_volunteer;
        $saveCampaign->tgl_buat = date('Y-m-d');
        $saveCampaign->save();

        return redirect('campaign/jenis')->with('status_success','Successfuly Add Kategori');

    }
}
