<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign\Jenisbiaya;

class JenisbiayaController extends Controller
{
    public function index()
    {
        $contents = [
            'jenis' => Jenisbiaya::all(),
        ];
        
        $pagecontent = view('contents.campaign.jenisbiaya.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'jenisbiaya',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_page()
    {
        $contents = [
        ];

        $pagecontent = view('contents.campaign.jenisbiaya.create', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'jenisbiaya',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_save(Request $request)
    {
        $active = FALSE;
        if($request->has('is_active')) {
            $active = TRUE;
        }

        $saveCampaign = new Jenisbiaya;
        $saveCampaign->nama = $request->nama;
        $saveCampaign->deskripsi = $request->deskripsi;
        $saveCampaign->is_active = $active;
        $saveCampaign->save();

        return redirect('campaign/jenisbiaya')->with('status_success','Successfuly Add Kategori');

    }

    public function update_page(Jenisbiaya $jenis)
    {
        $contents = [
            'jenis' => Jenisbiaya::where('id_jenis_biaya', $jenis->id_jenis_biaya)->first(),
        ];


        $pagecontent = view('contents.campaign.jenisbiaya.update', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'jenisbiaya',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);
    }

    public function update_save(Jenisbiaya $jenis ,Request $request)
    {
        $active = FALSE;
        if($request->has('is_active')) {
            $active = TRUE;
        }

        $saveCampaign = Jenisbiaya::where('id_jenis_biaya', $jenis->id_jenis_biaya)->first();
        $saveCampaign->nama = $request->nama;
        $saveCampaign->deskripsi = $request->deskripsi;
        $saveCampaign->is_active = $active;
        $saveCampaign->save();

        return redirect('campaign/jenisbiaya')->with('status_success','Successfuly Add Kategori');

    }

}
