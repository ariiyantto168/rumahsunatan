<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign\Kategori;

class KategoriController extends Controller
{
    public function index()
    {
        $contents = [
            'kategori' => Kategori::all(),
        ];

        
        $pagecontent = view('contents.campaign.kategori.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'kategori',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_page()
    {
        $contents = [
        ];

        $pagecontent = view('contents.campaign.kategori.create', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'kategori',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);
    }

    public function create_save(Request $request)
    {
        $active = FALSE;
        if($request->has('is_active')) {
            $active = TRUE;
        }

        $saveCampaign = new Kategori;
        $saveCampaign->nama = $request->nama;
        $saveCampaign->deskripsi = $request->deskripsi;
        $saveCampaign->is_active = $active;
        $saveCampaign->save();

        return redirect('campaign/kategori')->with('status_success','Successfuly Add Kategori');

    }

    public function update_page(Kategori $kategori)
    {
        $contents = [
            'kategori' => Kategori::where('id_kategori', $kategori->id_kategori)->first(),
        ];

        $pagecontent = view('contents.campaign.kategori.update', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'kategori',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);
    }

    public function update_save(Kategori $kategori ,Request $request)
    {
        $active = FALSE;
        if($request->has('is_active')) {
            $active = TRUE;
        }

        $saveCampaign = Kategori::where('id_kategori', $kategori->id_kategori)->first();
        $saveCampaign->nama = $request->nama;
        $saveCampaign->deskripsi = $request->deskripsi;
        $saveCampaign->is_active = $active;
        $saveCampaign->save();

        return redirect('campaign/kategori')->with('status_success','Successfuly Add Kategori');

    }


}
