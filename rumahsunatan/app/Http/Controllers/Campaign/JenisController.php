<?php

namespace App\Http\Controllers\Campaign;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Campaign\Jenis;

class JenisController extends Controller
{
    public function index()
    {
        $contents = [
            'jenis' => Jenis::all(),
        ];
        
        $pagecontent = view('contents.campaign.jenis.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'jenis',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_page()
    {
        $contents = [
        ];

        $pagecontent = view('contents.campaign.jenis.create', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'jenis',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);

    }

    public function create_save(Request $request)
    {
        $active = FALSE;
        if($request->has('is_active')) {
            $active = TRUE;
        }

        $saveCampaign = new Jenis;
        $saveCampaign->nama = $request->nama;
        $saveCampaign->deskripsi = $request->deskripsi;
        $saveCampaign->is_active = $active;
        $saveCampaign->save();

        return redirect('campaign/jenis')->with('status_success','Successfuly Add Kategori');

    }

    public function update_page(Jenis $jenis)
    {
        $contents = [
            'jenis' => Jenis::where('id_jenis', $jenis->id_jenis)->first(),
        ];


        $pagecontent = view('contents.campaign.jenis.update', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Campaign Kategori',
            'menu' => 'campaign',
            'submenu' => 'jenis',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);
    }

    public function update_save(Jenis $jenis ,Request $request)
    {
        $active = FALSE;
        if($request->has('is_active')) {
            $active = TRUE;
        }

        $saveCampaign = Jenis::where('id_jenis', $jenis->id_jenis)->first();
        $saveCampaign->nama = $request->nama;
        $saveCampaign->deskripsi = $request->deskripsi;
        $saveCampaign->is_active = $active;
        $saveCampaign->save();

        return redirect('campaign/jenis')->with('status_success','Successfuly Add Kategori');

    }
}
