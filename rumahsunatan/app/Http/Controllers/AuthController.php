<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Pengguna;
use App\Models\Role;

class AuthController extends Controller
{
    public function login()
    {
        return view('contents.login.index');
    }

    public function do_login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        

        if (Auth::guard('pengguna')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $datauser = Auth::guard('pengguna')->user();

            $pengguna = Pengguna::with(['role'])
                                ->where('id_pengguna', $datauser->id_pengguna)
                                ->first();
            $alldata = [
                'id_pengguna' => $datauser->id_pengguna,
                'name' => $datauser->name,
                'email' => $datauser->email,
                'role' => $pengguna->role->role,
            ];

            if (!empty($pengguna->role->role === 'admin')) {
                Session::put('pengguna',$alldata);
                return redirect('admin');
            }elseif (!empty($pengguna->role->role === 'pengguna')) {
                Session::put('pengguna',$alldata);
                return redirect('pengguna');
            }
        } 
        else {
            return 'auth fail';
        }


    }

    protected function get_user_privileges($idusers)
    {
        $data = [];
        //get privileges
        $privileges = Privileges::select('module_code')
            ->where('idusers',$idusers)
            ->get();

        //collect data
        foreach ($privileges as $pvl) {
            array_push($data, $pvl->module_code);
        }
        return $data;
    }

    public function logout(){
        Session::flush();
        Auth::logout();
        return redirect('login');
    }


}
