<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function admin()
    {
        $contents = [
        ];

        
        $pagecontent = view('contents.admin.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Home',
            'menu' => 'home',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.masterpage', $pagemain);
    }

    public function pengguna()
    {
        $contents = [
        ];

        
        $pagecontent = view('contents.pengguna.index', $contents);

    	//masterpage
        $pagemain = array(
            'title' => 'Home',
            'menu' => 'home',
            'submenu' => '',
            'pagecontent' => $pagecontent,
        );

        return view('contents.penggunapage', $pagemain);
    }

}
